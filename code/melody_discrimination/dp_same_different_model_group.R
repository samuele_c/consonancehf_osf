library(psyphy)
library(runjags)
library(coda)
library(gtools)
library(ggplot2)
source("../DBDA2E-utilities_SC.R")
source("../geom_flat_violin.R")

numSavedSteps = 25000
thinSteps = 10
updateMCMC = TRUE ## whether to run MCMC or use previously stored results

## Load the data file 
dat = read.table("../../datasets/melody_discrimination.csv", header=T, sep=",")
dat$listener = factor(dat$listener, levels=mixedsort(levels(dat$listener)))

## Create `results` directory if it doesn't exists
resDir = "../../results/melody_discrimination/"
dir.create(resDir, recursive=T)
dir.create(paste0(resDir, "diagnostics/"), recursive=T)

## JAGS model code
modelString = "
model {
   sqrt2 <- sqrt(2)

   for (s in 1:nSubj){
      nHits[s] ~ dbin(hitRate[s], nDifferent[s])
      nFA[s] ~ dbin(FARate[s], nSame[s])
      hitRate[s] <- phi((dp[s]-k[s])/sqrt2) + phi((-dp[s]-k[s])/sqrt2)
      FARate[s] <- 2*phi(-k[s]/sqrt2)

      dp[s] ~ dlnorm(logdpMu, 1/logdpSigma^2)
      k[s] ~ dunif(0, 10)
   }

   logdpMu ~ dlnorm(logmeandp, 1/(logsddp*5)^2) 
   logdpSigma ~ dgamma(logsigmadpShRa[1], logsigmadpShRa[2])
   dpMu <- exp(logdpMu)
   dpSigma <- exp(logdpSigma) 

}
"

## write model code in a temporary file
fPath = tempfile(fileext=".jag")
writeLines(modelString, con=fPath)

## prepare the data for shipping to JAGS
nHits = dat$nCorrDifferent
nFA = dat$nTotSame - dat$nCorrSame
nSame = dat$nTotSame
nDifferent = dat$nTotDifferent
nSubj = length(dat$listener)
meandp = mean(dat$dprime)
logmeandp = mean(log(dat$dprime))
sddp = sd(dat$dprime)
logsddp = sd(log(dat$dprime))

sigmadpShRa = unlist(gammaShRaFromModeSD(mode=sddp/2, sd=2*sddp))
logsigmadpShRa = unlist(gammaShRaFromModeSD(mode=logsddp/2, sd=2*logsddp))


dataList = list(
    nHits=nHits,
    nFA=nFA,
    nSame=nSame,
    nDifferent=nDifferent,
    nSubj = nSubj,
    meandp=meandp,
    sddp = sddp,
    sigmadpShRa = sigmadpShRa,
    logmeandp=logmeandp,
    logsddp=logsddp,
    logsigmadpShRa = logsigmadpShRa
)

parameters = c("dp", "dpMu", "hitRate", "FARate") # The parameters to be monitored
adaptSteps = 500             # Number of steps to adapt the samplers
burnInSteps = 1000            # Number of steps to burn-in the chains
nChains = 4                   # nChains should be 2 or more for diagnostics 

## set random seeds for reproducibility
initsFunction = function(chain){
    .RNG.seed = c(1, 2, 3, 4)[chain]
    .RNG.name = c("base::Super-Duper",
                  "base::Wichmann-Hill",
                  "base::Marsaglia-Multicarry",
                  "base::Mersenne-Twister")[chain]
    return(list(.RNG.seed=.RNG.seed,
                .RNG.name=.RNG.name))
}

if (updateMCMC == TRUE){
    ## run MCMC
    runJagsOut = run.jags(method="parallel",
                          model=fPath,
                          monitor=parameters,
                          data=dataList,
                          inits=initsFunction,
                          n.chains=nChains,
                          adapt=adaptSteps,
                          burnin=burnInSteps,
                          sample=ceiling(numSavedSteps/nChains),
                          thin=thinSteps,
                          summarise=FALSE,
                          plots=FALSE)
    codaSamples = as.mcmc.list(runJagsOut)
    ## store MCMC chains
    saveRDS(codaSamples, file=paste0(resDir, "mcmcCoda.RDS"))
} else {
    ## load previously stored MCMC chains
    codaSamples = readRDS(file=paste0(resDir, "mcmcCoda.RDS"))
}

mcmcMat = as.matrix(codaSamples, chains=T)

dpmc = numeric()
lis = character()
dpMode = numeric()
dpHDILow = numeric()
dpHDIHigh = numeric()

## plot posterior distribution of d' for each listener and at
## the group level, and get mode and 95% HDIs from posteriors
pdf(paste0(resDir, "posterior.pdf"))
par(mfrow=c(3,4))
for (i in 1:nSubj){
    thisChain = mcmcMat[,paste0("dp[", i, "]")]
    lis = c(lis, rep(as.character(dat$listener[i]), length(thisChain)))
    dpmc = c(dpmc, thisChain)
    thisSumm = summarizePost(mcmcMat[,paste0("dp[", i, "]")])
    dpMode = c(dpMode, thisSumm["Mode"])
    dpHDILow = c(dpHDILow, thisSumm["HDIlow"])
    dpHDIHigh = c(dpHDIHigh, thisSumm["HDIhigh"])
    plotPost(mcmcMat[,paste0("dp[", i, "]")])
}
par(mfrow=c(2,2))
plotPost(mcmcMat[,paste0("dpMu")])
dev.off()

## store MCMC chains in a dataframe (for ggplot2 plots)
mcmcFrame = data.frame(listener=lis, dp=dpmc)
mcmcFrame$listener = factor(mcmcFrame$listener, levels=mixedsort(levels(mcmcFrame$listener)), ordered=T)

## store posterior summaries in a dataframe
df = data.frame(listener=levels(dat$listener), dpMode=dpMode, dpHDILow=dpHDILow, dpHDIHigh=dpHDIHigh)
df$listener = factor(df$listener, levels=mixedsort(levels(df$listener)), ordered=T)


mcmcFrameMu = data.frame(listener="Mean", dp=mcmcMat[,"dpMu"])
thisSumm = summarizePost(mcmcMat[,"dpMu"])
dfMu = data.frame(listener="Mean", dpMode=thisSumm["Mode"], dpHDILow=thisSumm["HDIlow"], dpHDIHigh=thisSumm["HDIhigh"])

## plot posterior mode and 95% HDI for each listener and
## at the group level
p = ggplot(df, aes(x=listener, y=dpMode))
p = p + geom_flat_violin(data=mcmcFrame, aes(x=listener, y=dp))
p = p + geom_point(size=2, shape=1)
p = p + geom_errorbar(aes(ymin=dpHDILow, ymax=dpHDIHigh), width=0.25)
p = p + theme_bw(base_size=12) + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + xlab(NULL) + ylab(expression(italic("d'")))
p = p + geom_flat_violin(data=mcmcFrameMu, aes(x=listener, y=dp))
p = p + geom_point(data=dfMu, size=2, shape=1)
p = p + geom_errorbar(data=dfMu, aes(ymin=dpHDILow, ymax=dpHDIHigh), width=0.25)
p = p + scale_x_discrete(limits=c(levels(df$listener), "Mean"))
ggsave(paste0(resDir, "dp.pdf"), p)


## save results
saveRDS(df, file = paste0(resDir, "df.RDS"))
saveRDS(dfMu, file = paste0(resDir, "dfMu.RDS"))
saveRDS(mcmcFrame, file = paste0(resDir, "mcmcFrame.RDS"))
saveRDS(mcmcFrameMu, file = paste0(resDir, "mcmcFrameMu.RDS"))
saveRDS(mcmcMat, file = paste0(resDir, "mcmcMat.RDS"))

## plot diagnostics
params = varnames(codaSamples)
for (param in params){
    pdf(paste0(resDir, "diagnostics/", param, ".pdf"))
    diagMCMC(codaSamples, param)
    dev.off()
}



