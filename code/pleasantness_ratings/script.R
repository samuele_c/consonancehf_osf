library(ggplot2)
source("Jags-Ymet-Xnom3fac-Within-MnormalHom-Robust.R")
source("../DBDA2E-utilities_DF.R")
numSavedSteps = 50000
thinSteps = 10
updateMCMC = TRUE ## whether to run MCMC or use previously stored results

## Load the data file 
dat = read.table(file="../../datasets/pleasantness_ratings.csv", sep=",", header=T)

resDir = "../../results/pleasantness_ratings/"
dir.create(resDir, recursive=T)
dir.create(paste0(resDir, "diagnostics/"), recursive=T)

## Specify the column names in the data file relevant to the analysis:
yName="zscore" ##standardized ratings
x1Name="interval" ##interval (m2, m3, TT, or P5)
x2Name="freq_reg" ##frequency region (low or high)
x3Name="shift" ## harmonicity (harmonic or inharmonic)
xSubjName = "listener"

if (updateMCMC == TRUE){
    ## run MCMC
    mcmcCoda = genMCMCAovThreeWayWtn(dat=dat, yName=yName, x1Name=x1Name, x2Name=x2Name, x3Name=x3Name, xSubjectName=xSubjName,
                                     numSavedSteps=numSavedSteps, thinSteps=thinSteps)
    ## store MCMC chains
    saveRDS(mcmcCoda, file=paste0(resDir, "mcmcCoda.RDS"))
    ## plot diagnostics
    parameterNames = varnames(mcmcCoda) # get all parameter names
    for (parName in parameterNames) {
        pdf(paste0(resDir, "diagnostics/", parName, ".pdf"))
        diagMCMC(codaObject=mcmcCoda , parName=parName)
        dev.off()
    }
} else {
    ## load previously stored MCMC chains
    mcmcCoda = readRDS(file=paste0(resDir, "mcmcCoda.RDS"))
}


mcmcMat = as.matrix(mcmcCoda, chains=T)

findLevNum = function(levName, fctr){
    xn = as.numeric(as.factor(fctr))
    levNum = xn[which(fctr==levName)[1]]
    return(levNum)
}

findLevLab = function(levNum, fctr){
    xn = as.numeric(as.factor(fctr))
    levLab = as.character(fctr[which(xn==levNum)[1]])
    return(levLab)
}
    
x1Levels = levels(as.factor(dat[,x1Name]))
x2Levels = levels(as.factor(dat[,x2Name]))
x3Levels = levels(as.factor(dat[,x3Name]))
xSubjLevels = levels(as.factor(dat[,xSubjName]))
mc = list()

for (x1Lev in x1Levels){
    x1n = findLevNum(x1Lev, dat[,x1Name])
    for (x2Lev in x2Levels){
        x2n = findLevNum(x2Lev, dat[,x2Name])
        for (x3Lev in x3Levels){
            x3n = findLevNum(x3Lev, dat[,x3Name])
            mc[[paste(x1Lev, x2Lev, x3Lev, sep="_")]] = mcmcMat[, "b0mu"] + mcmcMat[, paste0("bW1mu[", x1n, "]")] + mcmcMat[, paste0("bW2mu[", x2n, "]")] + mcmcMat[, paste0("bW3mu[", x3n, "]")] +
                mcmcMat[, paste0("bW1xW2mu[", x1n, ",", x2n, "]")] + mcmcMat[, paste0("bW1xW3mu[", x1n, ",", x3n, "]")] + mcmcMat[, paste0("bW2xW3mu[", x2n, ",", x3n, "]")] +
                mcmcMat[, paste0("bW1xW2xW3mu[", x1n, ",", x2n, ",", x3n, "]")]

        }
    }
}

df_post_each = summarizePostListDF(list(mc[["Minor Second_Low_Harmonic"]],
                                        mc[["Minor Third_Low_Harmonic"]],
                                        mc[["Tritone_Low_Harmonic"]],
                                        mc[["Perfect Fifth_Low_Harmonic"]],
                                        mc[["Minor Second_Low_Shifted"]],
                                        mc[["Minor Third_Low_Shifted"]],
                                        mc[["Tritone_Low_Shifted"]],
                                        mc[["Perfect Fifth_Low_Shifted"]],
                                        mc[["Minor Second_High_Harmonic"]],
                                        mc[["Minor Third_High_Harmonic"]],
                                        mc[["Tritone_High_Harmonic"]],
                                        mc[["Perfect Fifth_High_Harmonic"]],
                                        mc[["Minor Second_High_Shifted"]],
                                        mc[["Minor Third_High_Shifted"]],
                                        mc[["Tritone_High_Shifted"]],
                                        mc[["Perfect Fifth_High_Shifted"]]),
                                   labels=c("Minor Second_Low_Harmonic",
                                        "Minor Third_Low_Harmonic",
                                        "Tritone_Low_Harmonic",
                                        "Perfect Fifth_Low_Harmonic",
                                        "Minor Second_Low_Shifted",
                                        "Minor Third_Low_Shifted",
                                        "Tritone_Low_Shifted",
                                        "Perfect Fifth_Low_Shifted",
                                        "Minor Second_High_Harmonic",
                                        "Minor Third_High_Harmonic",
                                        "Tritone_High_Harmonic",
                                        "Perfect Fifth_High_Harmonic",
                                        "Minor Second_High_Shifted",
                                        "Minor Third_High_Shifted",
                                        "Tritone_High_Shifted",
                                        "Perfect Fifth_High_Shifted"))
df_post_each$interval = factor(rep(c("Minor Second", "Minor Third", "Tritone", "Perfect Fifth"), 4), levels=c("Minor Second", "Minor Third", "Tritone", "Perfect Fifth"))
df_post_each$freq_reg = factor(rep(c("Low", "High"), each=8), levels=c("Low", "High"))
df_post_each$shift = factor(rep(rep(c("Harmonic", "Inharmonic"), each=4), 2), levels=c("Harmonic", "Inharmonic"))

df_post_diff = summarizePostListDF(list(mc[["Minor Second_Low_Harmonic"]]-mc[["Minor Second_Low_Shifted"]],
                                        mc[["Minor Third_Low_Harmonic"]]-mc[["Minor Third_Low_Shifted"]],
                                        mc[["Tritone_Low_Harmonic"]]-mc[["Tritone_Low_Shifted"]],
                                        mc[["Perfect Fifth_Low_Harmonic"]]-mc[["Perfect Fifth_Low_Shifted"]],
                                        mc[["Minor Second_High_Harmonic"]]-mc[["Minor Second_High_Shifted"]],
                                        mc[["Minor Third_High_Harmonic"]]-mc[["Minor Third_High_Shifted"]],
                                        mc[["Tritone_High_Harmonic"]]-mc[["Tritone_High_Shifted"]],
                                        mc[["Perfect Fifth_High_Harmonic"]]-mc[["Perfect Fifth_High_Shifted"]]),
                                   labels=c("Minor Second_Low",
                                            "Minor Third_Low",
                                            "Tritone_Low",
                                            "Perfect Fifth_Low",
                                            "Minor Second_High",
                                            "Minor Third_High",
                                            "Tritone_High",
                                            "Perfect Fifth_High"))
df_post_diff$freq_reg = factor(rep(c("Low", "High"), each=4), levels=c("Low", "High"))
df_post_diff$interval = factor(rep(c("Minor Second", "Minor Third", "Tritone", "Perfect Fifth"), 2), levels=c("Minor Second", "Minor Third", "Tritone", "Perfect Fifth"))
                                     
df_post_contrasts_cons_diss_each = summarizePostListDF(list(
    mc[["Perfect Fifth_Low_Harmonic"]] - mc[["Minor Second_Low_Harmonic"]],
    mc[["Perfect Fifth_Low_Harmonic"]] - mc[["Tritone_Low_Harmonic"]],
    mc[["Minor Third_Low_Harmonic"]] - mc[["Minor Second_Low_Harmonic"]],
    mc[["Minor Third_Low_Harmonic"]] - mc[["Tritone_Low_Harmonic"]],

    mc[["Perfect Fifth_High_Harmonic"]] - mc[["Minor Second_High_Harmonic"]],
    mc[["Perfect Fifth_High_Harmonic"]] - mc[["Tritone_High_Harmonic"]],
    mc[["Minor Third_High_Harmonic"]] - mc[["Minor Second_High_Harmonic"]],
    mc[["Minor Third_High_Harmonic"]] - mc[["Tritone_High_Harmonic"]],

    mc[["Perfect Fifth_Low_Shifted"]] - mc[["Minor Second_Low_Shifted"]],
    mc[["Perfect Fifth_Low_Shifted"]] - mc[["Tritone_Low_Shifted"]],
    mc[["Minor Third_Low_Shifted"]] - mc[["Minor Second_Low_Shifted"]],
    mc[["Minor Third_Low_Shifted"]] - mc[["Tritone_Low_Shifted"]],

    mc[["Perfect Fifth_High_Shifted"]] - mc[["Minor Second_High_Shifted"]],
    mc[["Perfect Fifth_High_Shifted"]] - mc[["Tritone_High_Shifted"]],
    mc[["Minor Third_High_Shifted"]] - mc[["Minor Second_High_Shifted"]],
    mc[["Minor Third_High_Shifted"]] - mc[["Tritone_High_Shifted"]]),

    labels = c("P5-m2LH", "P5-TTLH", "m3-m2LH", "m3-TTLH",
               "P5-m2HH", "P5-TTHH", "m3-m2HH", "m3-TTHH",
               "P5-m2LS", "P5-TTLS", "m3-m2LS", "m3-TTLS",
               "P5-m2HS", "P5-TTHS", "m3-m2HS", "m3-TTHS"))

df_post_contrasts_cons_diss_each$cntr = factor(rep(c("P5 - m2", "P5 - TT", "m3 - m2", "m3 - TT"), 4), levels=c("P5 - m2", "P5 - TT", "m3 - m2", "m3 - TT"))
df_post_contrasts_cons_diss_each$freq_reg = factor(rep(rep(c("Low", "High"), each=4), 2), levels=c("Low", "High"))
df_post_contrasts_cons_diss_each$shift = factor(rep(c("Harmonic", "Inharmonic"), each=8), levels=c("Harmonic", "Inharmonic"))


##
df_post_pairwise_by_freq_harm = summarizePostListDF(list(
    mc[["Perfect Fifth_Low_Harmonic"]] - mc[["Minor Second_Low_Harmonic"]],
    mc[["Perfect Fifth_Low_Harmonic"]] - mc[["Tritone_Low_Harmonic"]],
    mc[["Perfect Fifth_Low_Harmonic"]] - mc[["Minor Third_Low_Harmonic"]],
    mc[["Minor Third_Low_Harmonic"]] - mc[["Minor Second_Low_Harmonic"]],
    mc[["Minor Third_Low_Harmonic"]] - mc[["Tritone_Low_Harmonic"]],
    mc[["Tritone_Low_Harmonic"]] - mc[["Minor Second_Low_Harmonic"]],

    mc[["Perfect Fifth_High_Harmonic"]] - mc[["Minor Second_High_Harmonic"]],
    mc[["Perfect Fifth_High_Harmonic"]] - mc[["Tritone_High_Harmonic"]],
    mc[["Perfect Fifth_High_Harmonic"]] - mc[["Minor Third_High_Harmonic"]],
    mc[["Minor Third_High_Harmonic"]] - mc[["Minor Second_High_Harmonic"]],
    mc[["Minor Third_High_Harmonic"]] - mc[["Tritone_High_Harmonic"]],
    mc[["Tritone_High_Harmonic"]] - mc[["Minor Second_High_Harmonic"]],

    mc[["Perfect Fifth_Low_Shifted"]] - mc[["Minor Second_Low_Shifted"]],
    mc[["Perfect Fifth_Low_Shifted"]] - mc[["Tritone_Low_Shifted"]],
    mc[["Perfect Fifth_Low_Shifted"]] - mc[["Minor Third_Low_Shifted"]],
    mc[["Minor Third_Low_Shifted"]] - mc[["Minor Second_Low_Shifted"]],
    mc[["Minor Third_Low_Shifted"]] - mc[["Tritone_Low_Shifted"]],
    mc[["Tritone_Low_Shifted"]] - mc[["Minor Second_Low_Shifted"]],

    mc[["Perfect Fifth_High_Shifted"]] - mc[["Minor Second_High_Shifted"]],
    mc[["Perfect Fifth_High_Shifted"]] - mc[["Tritone_High_Shifted"]],
    mc[["Perfect Fifth_High_Shifted"]] - mc[["Minor Third_High_Shifted"]],
    mc[["Minor Third_High_Shifted"]] - mc[["Minor Second_High_Shifted"]],
    mc[["Minor Third_High_Shifted"]] - mc[["Tritone_High_Shifted"]],
    mc[["Tritone_High_Shifted"]] - mc[["Minor Second_High_Shifted"]]),

    labels = c("P5-m2LH", "P5-TTLH", "P5-m3LH", "m3-m2LH", "m3-TTLH", "TT-m2LH",
               "P5-m2HH", "P5-TTHH", "P5-m3HH", "m3-m2HH", "m3-TTHH", "TT-m2HH",
               "P5-m2LS", "P5-TTLS", "P5-m3LS", "m3-m2LS", "m3-TTLS", "TT-m2LS",
               "P5-m2HS", "P5-TTHS", "P5-m3HS", "m3-m2HS", "m3-TTHS", "TT-m2HS"))

df_post_pairwise_by_freq_harm$cntr = factor(rep(c("P5 - m2", "P5 - TT", "P5 - m3", "m3 - m2", "m3 - TT", "TT - m2"), 4), levels=c("P5 - m2", "P5 - TT", "P5 - m3", "m3 - m2", "m3 - TT", "TT - m2"))
df_post_pairwise_by_freq_harm$freq_reg = factor(rep(rep(c("Low", "High"), each=6), 2), levels=c("Low", "High"))
df_post_pairwise_by_freq_harm$shift = factor(rep(c("Harmonic", "Inharmonic"), each=12), levels=c("Harmonic", "Inharmonic"))
##
    
##
mcSubj = list()
for (xSubjLev in xSubjLevels){
    xSubjn = findLevNum(xSubjLev, dat[,xSubjName])
    for (x1Lev in x1Levels){
        x1n = findLevNum(x1Lev, dat[,x1Name])
        for (x2Lev in x2Levels){
            x2n = findLevNum(x2Lev, dat[,x2Name])
            for (x3Lev in x3Levels){
                x3n = findLevNum(x3Lev, dat[,x3Name])
                mcSubj[[paste(xSubjLev, x1Lev, x2Lev, x3Lev, sep="_")]] = mcmcMat[, paste0("b0[", xSubjn, "]")] + mcmcMat[, paste0("bW1[", xSubjn, ",", x1n, "]")] + mcmcMat[, paste0("bW2[", xSubjn, ",", x2n, "]")] + mcmcMat[, paste0("bW3[",  xSubjn, ",", x3n, "]")] +
                    mcmcMat[, paste0("bW1xW2[",  xSubjn, ",", x1n, ",", x2n, "]")] + mcmcMat[, paste0("bW1xW3[",  xSubjn, ",", x1n, ",", x3n, "]")] + mcmcMat[, paste0("bW2xW3[",  xSubjn, ",", x2n, ",", x3n, "]")] +
                        mcmcMat[, paste0("bW1xW2xW3[",  xSubjn, ",", x1n, ",", x2n, ",", x3n, "]")]
            }
        }
    }
}




pdf(paste0(resDir, "contrasts.pdf"))
par(mfrow=c(2,2))
##Harmonic low frequency
cntrLowFreqHarmChain = (mc[["Minor Third_Low_Harmonic"]] + mc[["Perfect Fifth_Low_Harmonic"]])/2 - (mc[["Minor Second_Low_Harmonic"]] + mc[["Tritone_Low_Harmonic"]])/2
cntrLowFreqHarm = plotPost(cntrLowFreqHarmChain, compVal=0, main="Low Freq. Harmonic")

##Shifted low frequency
cntrLowFreqShiftChain = (mc[["Minor Third_Low_Shifted"]] + mc[["Perfect Fifth_Low_Shifted"]])/2 - (mc[["Minor Second_Low_Shifted"]] + mc[["Tritone_Low_Shifted"]])/2
cntrLowFreqShift = plotPost(cntrLowFreqShiftChain, compVal=0, main="Low Freq. Shifted")

##Harmonic high frequency
cntrHighFreqHarmChain = (mc[["Minor Third_High_Harmonic"]] + mc[["Perfect Fifth_High_Harmonic"]])/2 - (mc[["Minor Second_High_Harmonic"]] + mc[["Tritone_High_Harmonic"]])/2
cntrHighFreqHarm = plotPost(cntrHighFreqHarmChain, compVal=0, main="High Freq. Harmonic")

##Shifted high frequency
cntrHighFreqShiftChain = (mc[["Minor Third_High_Shifted"]] + mc[["Perfect Fifth_High_Shifted"]])/2 - (mc[["Minor Second_High_Shifted"]] + mc[["Tritone_High_Shifted"]])/2
cntrHighFreqShift =  plotPost(cntrHighFreqShiftChain, compVal=0, main="High Freq. Shifted")

## Low freq - HighFreq Harm
cntrLowFreqvsHighFreqHarmChain = ((mc[["Minor Third_Low_Harmonic"]] + mc[["Perfect Fifth_Low_Harmonic"]])/2 - (mc[["Minor Second_Low_Harmonic"]] + mc[["Tritone_Low_Harmonic"]])/2) -
            ((mc[["Minor Third_High_Harmonic"]] + mc[["Perfect Fifth_High_Harmonic"]])/2 - (mc[["Minor Second_High_Harmonic"]] + mc[["Tritone_High_Harmonic"]])/2)
cntrLowFreqvsHighFreqHarm = plotPost(cntrLowFreqvsHighFreqHarmChain, compVal=0, main="Cons. Pref Low Harm. - High Harm")

## Harm vs Shift Low
cntrHarmvsShiftLowChain = ((mc[["Minor Third_Low_Harmonic"]] + mc[["Perfect Fifth_Low_Harmonic"]])/2 - (mc[["Minor Second_Low_Harmonic"]] + mc[["Tritone_Low_Harmonic"]])/2) -
             ((mc[["Minor Third_Low_Shifted"]] + mc[["Perfect Fifth_Low_Shifted"]])/2 - (mc[["Minor Second_Low_Shifted"]] + mc[["Tritone_Low_Shifted"]])/2)
cntrHarmvsShiftLow = plotPost(cntrHarmvsShiftLowChain, compVal=0, main="Cons. Pref Low Harm. - Low Shifted")

## Harm vs Shift High
cntrHarmvsShiftHighChain = ((mc[["Minor Third_High_Harmonic"]] + mc[["Perfect Fifth_High_Harmonic"]])/2 - (mc[["Minor Second_High_Harmonic"]] + mc[["Tritone_High_Harmonic"]])/2) -
             ((mc[["Minor Third_High_Shifted"]] + mc[["Perfect Fifth_High_Shifted"]])/2 - (mc[["Minor Second_High_Shifted"]] + mc[["Tritone_High_Shifted"]])/2)
cntrHarmvsShiftHigh = plotPost(cntrHarmvsShiftHighChain, compVal=0, main="Cons. Pref High Harm. - High Shifted")
dev.off()

chainVector = c(cntrLowFreqHarmChain, cntrHighFreqHarmChain, cntrLowFreqShiftChain, cntrHighFreqShiftChain, cntrLowFreqvsHighFreqHarmChain, cntrHarmvsShiftLowChain, cntrHarmvsShiftHighChain)
chainVectorLabel = factor(rep(c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Inharm.", "High Freq. Inharm.", "Low vs High Freq. Harm.", "Harm. vs Inharm. \n@Low Freq.", "Harm. vs Inharm. \n@High Freq."), each=length(cntrLowFreqHarmChain)), levels=c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Inharm.", "High Freq. Inharm.", "Low vs High Freq. Harm.", "Harm. vs Inharm. \n@Low Freq.", "Harm. vs Inharm. \n@High Freq."), ordered=T)
cntrChainFrame = data.frame(val=chainVector, label=chainVectorLabel)


cntrFrame = as.data.frame(cntrLowFreqHarm)
cntrFrame = rbind(cntrFrame, cntrHighFreqHarm, cntrLowFreqShift, cntrHighFreqShift, cntrLowFreqvsHighFreqHarm, cntrHarmvsShiftLow, cntrHarmvsShiftHigh)
cntrFrame$label = factor(c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Inharm.", "High Freq. Inharm.", "Low vs High Freq. Harm.", "Harm. vs Inharm. \n@Low Freq.", "Harm. vs Inharm. \n@High Freq."), levels=c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Inharm.", "High Freq. Inharm.", "Low vs High Freq. Harm.", "Harm. vs Inharm. \n@Low Freq.", "Harm. vs Inharm. \n@High Freq."), ordered=T)



p = ggplot(cntrFrame, aes(y=mode, x=label))
p = p + geom_point(size=2, shape=1)
p = p + geom_errorbar(aes(ymin=hdiLow, ymax=hdiHigh), width=0.25)
p = p + theme_bw(base_size=12) + geom_hline(yintercept=0, linetype=2) + coord_flip()
p = p + ylab(NULL) + xlab(NULL)
ggsave(paste0(resDir, "CIs.pdf"), p)
#p = p+geom_flat_violin(data=cntrChainFrame, aes(x=label, y=val))



saveRDS(mc, file=paste0(resDir, "mcmc_cells.RDS"))
saveRDS(cntrFrame, file=paste0(resDir, "contrast_frame.RDS"))
saveRDS(cntrChainFrame, file=paste0(resDir, "contrasts_chain_frame.RDS"))


############
##CONTRASTS SUBJECTS

cntrSubjLowFreqHarmChain = list()
cntrSubjLowFreqHarm = list()
cntrSubjLowFreqShiftChain = list()
cntrSubjLowFreqShift = list()
cntrSubjHighFreqHarmChain = list()
cntrSubjHighFreqHarm = list()
cntrSubjHighFreqShiftChain = list()
cntrSubjHighFreqShift = list()
cntrSubjLowFreqvsHighFreqHarmChain = list()
cntrSubjLowFreqvsHighFreqHarm = list()
cntrSubjHarmvsShiftLowChain = list()
cntrSubjHarmvsShiftLow = list()
cntrSubjHarmvsShiftHighChain = list()
cntrSubjHarmvsShiftHigh = list()
for (xSubjLev in xSubjLevels){
    xSubjn = findLevNum(xSubjLev, dat[,xSubjName])
    
    pdf(paste0(resDir, "contrasts_", xSubjLev, ".pdf"))
    par(mfrow=c(2,2))
    ##Harmonic low frequency
    cntrSubjLowFreqHarmChain[[xSubjLev]] = (mcSubj[[paste0(xSubjLev,"_Minor Third_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_Low_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_Low_Harmonic")]])/2
    cntrSubjLowFreqHarm[[xSubjLev]] = plotPost(cntrSubjLowFreqHarmChain[[xSubjLev]], compVal=0, main="Low Freq. Harmonic")

    ##Shifted low frequency
    cntrSubjLowFreqShiftChain[[xSubjLev]] = (mcSubj[[paste0(xSubjLev,"_Minor Third_Low_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_Low_Shifted")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_Low_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Tritone_Low_Shifted")]])/2
    cntrSubjLowFreqShift[[xSubjLev]] = plotPost(cntrSubjLowFreqShiftChain[[xSubjLev]], compVal=0, main="Low Freq. Shifted")
    
    ##Harmonic high frequency
    cntrSubjHighFreqHarmChain[[xSubjLev]] = (mcSubj[[paste0(xSubjLev,"_Minor Third_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_High_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_High_Harmonic")]])/2
    cntrSubjHighFreqHarm[[xSubjLev]] = plotPost(cntrSubjHighFreqHarmChain[[xSubjLev]], compVal=0, main="High Freq. Harmonic")
    
    ##Shifted high frequency
    cntrSubjHighFreqShiftChain[[xSubjLev]] = (mcSubj[[paste0(xSubjLev,"_Minor Third_High_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_High_Shifted")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_High_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Tritone_High_Shifted")]])/2
    cntrSubjHighFreqShift[[xSubjLev]] =  plotPost(cntrSubjHighFreqShiftChain[[xSubjLev]], compVal=0, main="High Freq. Shifted")
    
    ## Low freq - HighFreq Harm
    cntrSubjLowFreqvsHighFreqHarmChain[[xSubjLev]] = ((mcSubj[[paste0(xSubjLev,"_Minor Third_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_Low_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_Low_Harmonic")]])/2) -
        ((mcSubj[[paste0(xSubjLev,"_Minor Third_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_High_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_High_Harmonic")]])/2)
    cntrSubjLowFreqvsHighFreqHarm[[xSubjLev]] = plotPost(cntrSubjLowFreqvsHighFreqHarmChain[[xSubjLev]], compVal=0, main="Cons. Pref Low Harm. - High Harm")
    
    ## Harm vs Shift Low
    cntrSubjHarmvsShiftLowChain[[xSubjLev]] = ((mcSubj[[paste0(xSubjLev,"_Minor Third_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_Low_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_Low_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_Low_Harmonic")]])/2) -
        ((mcSubj[[paste0(xSubjLev,"_Minor Third_Low_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_Low_Shifted")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_Low_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Tritone_Low_Shifted")]])/2)
    cntrSubjHarmvsShiftLow[[xSubjLev]] = plotPost(cntrSubjHarmvsShiftLowChain[[xSubjLev]], compVal=0, main="Cons. Pref Low Harm. - Low Shifted")
    
    ## Harm vs Shift High
    cntrSubjHarmvsShiftHighChain[[xSubjLev]] = ((mcSubj[[paste0(xSubjLev,"_Minor Third_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_High_Harmonic")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_High_Harmonic")]] + mcSubj[[paste0(xSubjLev,"_Tritone_High_Harmonic")]])/2) -
        ((mcSubj[[paste0(xSubjLev,"_Minor Third_High_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Perfect Fifth_High_Shifted")]])/2 - (mcSubj[[paste0(xSubjLev,"_Minor Second_High_Shifted")]] + mcSubj[[paste0(xSubjLev,"_Tritone_High_Shifted")]])/2)
    cntrSubjHarmvsShiftHigh[[xSubjLev]] = plotPost(cntrSubjHarmvsShiftHighChain[[xSubjLev]], compVal=0, main="Cons. Pref High Harm. - High Shifted")
    dev.off()
}

chainVectorSubj = numeric()
chainVectorLabelSubj = character()
subjVector = character()
for (xSubjLev in xSubjLevels){
    chainVectorSubj = c(chainVectorSubj, c(cntrSubjLowFreqHarmChain[[xSubjLev]], cntrSubjHighFreqHarmChain[[xSubjLev]], cntrSubjLowFreqShiftChain[[xSubjLev]], cntrSubjHighFreqShiftChain[[xSubjLev]], cntrSubjLowFreqvsHighFreqHarmChain[[xSubjLev]], cntrSubjHarmvsShiftLowChain[[xSubjLev]], cntrSubjHarmvsShiftHighChain[[xSubjLev]]))
    chainVectorLabelSubj = c(chainVectorLabelSubj, factor(rep(c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Shift.", "High Freq. Shift.", "Low vs High Freq. Harm.", "Harm. vs Shift. Low Freq", "Harm. vs Shift. High Freq"), each=length(cntrSubjLowFreqHarmChain[[xSubjLev]])), levels=c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Shift.", "High Freq. Shift.", "Low vs High Freq. Harm.", "Harm. vs Shift. Low Freq", "Harm. vs Shift. High Freq"), ordered=T))
    subjVector = c(subjVector, rep(xSubjLev, length(cntrSubjLowFreqHarmChain[[xSubjLev]])))
}
cntrSubjChainFrame = data.frame(listener=subjVector, val=chainVectorSubj, label=chainVectorLabelSubj)

cntrSubjFrame = data.frame()
for (xSubjLev in xSubjLevels){
    thisCntrSubjFrame = as.data.frame(cntrSubjLowFreqHarm[[xSubjLev]])
    thisCntrSubjFrame = rbind(thisCntrSubjFrame, cntrSubjHighFreqHarm[[xSubjLev]], cntrSubjLowFreqShift[[xSubjLev]], cntrSubjHighFreqShift[[xSubjLev]], cntrSubjLowFreqvsHighFreqHarm[[xSubjLev]], cntrSubjHarmvsShiftLow[[xSubjLev]], cntrSubjHarmvsShiftHigh[[xSubjLev]])
    thisCntrSubjFrame$label = factor(c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Shift.", "High Freq. Shift.", "Low vs High Freq. Harm.", "Harm. vs Shift. Low Freq", "Harm. vs Shift. High Freq"), levels=c("Low Freq. Harm.", "High Freq. Harm.", "Low Freq. Shift.", "High Freq. Shift.", "Low vs High Freq. Harm.", "Harm. vs Shift. Low Freq", "Harm. vs Shift. High Freq"), ordered=T)
    thisCntrSubjFrame$listener = xSubjLev
    cntrSubjFrame = rbind(cntrSubjFrame, thisCntrSubjFrame)
}

p = ggplot(cntrSubjFrame, aes(y=mode, x=label))
p = p + geom_point(size=2, shape=1)
p = p + geom_errorbar(aes(ymin=hdiLow, ymax=hdiHigh), width=0.25)
p = p + theme_bw(base_size=12) + geom_hline(yintercept=0, linetype=2) + coord_flip()
p = p + facet_wrap(~listener)
p = p + ylab(NULL) + xlab(NULL)
ggsave(paste0(resDir, "CIs_listener.pdf"), p)

zscoreText = expression(paste(italic(z), " Score"))
cls4 = c("black", "slateblue") #brewer.pal(8, "Accent")
p = ggplot(df_post_each, aes(x=interval, y=Mode, color=freq_reg, shape=freq_reg, group=freq_reg))
p = p + geom_point(position=position_dodge(0.5), size=2) + facet_wrap(~shift) + geom_line(position=position_dodge(0.5))
p = p +  scale_color_manual(name="Frequency Region", values=cls4)
p = p +  scale_shape_manual(name="Frequency Region", values=c(16,17))
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), position=position_dodge(0.5), width=0.5)
p = p + xlab("Interval") + ylab(zscoreText) + theme_bw()
p = p + theme(axis.text.x = element_text(angle = 45, hjust = 1))
ggsave(filename=paste0(resDir, "posterior_ratings_each.pdf"), width=3.307*2, height=4)

thisYLab = expression(paste("Harmonic - Inharmonic (", italic(z), " Score)"))
p = ggplot(df_post_diff, aes(x=interval, y=Mode, color=freq_reg, shape=freq_reg, group=freq_reg))
p = p + geom_point(position=position_dodge(0.5), size=2) + geom_line(position=position_dodge(0.5))
p = p +  scale_color_manual(name="Frequency Region", values=cls4)
p = p +  scale_shape_manual(name="Frequency Region", values=c(16,17))
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), position=position_dodge(0.5), width=0.5)
p = p + xlab(NULL) + ylab(thisYLab) + theme_bw()
p = p + theme(axis.text.x = element_text(angle = 45, hjust = 1))
p = p + theme(legend.position="top")
ggsave(filename=paste0(resDir, "posterior_ratings_diff_harm_shift.pdf"), width=3.307, height=4)


##CIs cons - diss each
p = ggplot(df_post_contrasts_cons_diss_each, aes(y=Mode, x=cntr, shape=shift, color=shift)) 
p = p + geom_point(size=2, shape=1, position=position_dodge(-0.5))
p = p + facet_wrap(~freq_reg)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0.25, position=position_dodge(-0.5))
p = p + theme_bw(base_size=12) + geom_hline(yintercept=0, linetype=2) + coord_flip()
p = p + ylab(NULL) + xlab(NULL)
ggsave(paste0(resDir, "CIs_cons_diss_each.pdf"), p)

##CIs pairwise
p = ggplot(df_post_pairwise_by_freq_harm, aes(y=Mode, x=cntr, shape=shift, color=shift)) 
p = p + geom_point(size=2, shape=1, position=position_dodge(-0.5))
p = p + facet_wrap(~freq_reg)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0.25, position=position_dodge(-0.5))
p = p + theme_bw(base_size=12) + geom_hline(yintercept=0, linetype=2) + coord_flip()
p = p + ylab(NULL) + xlab(NULL)
ggsave(paste0(resDir, "CIs_pairwise_by_freq_harm.pdf"), p)

##
saveRDS(mcSubj, file=paste0(resDir, "mcmc_cells_subj.RDS"))
saveRDS(cntrSubjFrame, file=paste0(resDir, "contrast_frame_subj.RDS"))
saveRDS(cntrSubjChainFrame, file=paste0(resDir, "contrasts_chain_frame_subj.RDS"))
saveRDS(df_post_contrasts_cons_diss_each, file=paste0(resDir, "contrast_cons_diss_each.RDS"))
saveRDS(df_post_pairwise_by_freq_harm, file=paste0(resDir, "df_post_pairwise_by_freq_harm.RDS"))
