Data are stored in the `datasets` folder. Model code in the `code` folder. Data dictionaries for the datasets with a short description of each variable are provided in the `data_dictionaries` folder. There is one data dictionary file for each dataset file, the data dictionary filename consists of the dataset filename plus the suffix `_data_dictionary`. Running the model code creates an additional "results" directory in the root folder where this README is located.

Datasets:
---------

 - `pleasantness_ratings.csv`: pleasantness ratings given by each listener on each trial of the rating test. The raw ratings are in the `rating` column. The standardized rating are in the `zscore` column.
 - `pleasantness_ratings_average.csv`: pleasantness ratings given by each listener and experimental condition, averaged over the eight trials run per condition.
 - `consonance_preference.csv`: difference between average ratings given to consonant (m3, P5), and dissonant (m2, TT) intervals by frequency region and harmonicity. The `ratingPref` column stores the difference in raw units. The `zscorePref` stores the difference in standardized units.
 - `melody_discrimination.csv`: performance in the same-different melody discrimination task. The number of correct and total responses for "same" and "different" trials are given for each listener. the "dprime" column here stores the *d'* value computed with the [`psyphy`](https://cran.r-project.org/web/packages/psyphy/index.html) R package.


Code:
------------

The files immediately under the `code` directory contain general utility functions, partly adapted from code in Kruschke (2014). The files in the folders nested under the `code` directory contain code specific to each experiment.

Pleasantness ratings: the file `Jags-Ymet-Xnom3fac-Within-MnormalHom-Robust.R` contains the JAGS model code. Functions in this file are invoked by code in the `script.R` file to run the analyses. 

Melody discrimination: the file `dp_same_different_model_group.R` contains JAGS and R code to compute *d'* in the same-different melody discrimination task.

Notes:
-------------

Running the JAGS code can take a long time. For this reason the files invoking the JAGS functions have an `updateMCMC` switch that allows loading previously stored results instead of running the Markov chain Monte Carlo. If there are no previously stored results the scripts will throw an error message, so make sure `updateMCMC` is set to `TRUE` the first time you're running the scripts.

The published analyses were run with R v3.6.1, JAGS v4.3.0. The file `sessionInfo.txt` within the `melody_discrimination`, and the `pleasantness_ratings` folders contains the output of the `sessionInfo()` R command that prints version information about R, the OS and attached or loaded packages used to run the published analyses.

References
----------

- Kruschke, J. K. (2014). *Doing Bayesian Data Analysis: A Tutorial with R, JAGS, and Stan*. (2nd ed.). London: Academid Press / Elsevier. 
